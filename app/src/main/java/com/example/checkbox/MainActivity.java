package com.example.checkbox;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.TextView;

public class MainActivity extends AppCompatActivity {
    private EditText numero1;
    private  EditText numero2;
    private CheckBox sumar;
    private CheckBox restar;
    private TextView resultado;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        numero1 = findViewById(R.id.txtnumero1);
        numero2 = findViewById(R.id.txtnumero2);
        sumar = findViewById(R.id.cbsumar);
        restar = findViewById(R.id.cbrestar);
        resultado = findViewById(R.id.tvresultado);

    }
    int num1 = Integer.parseInt(numero1.getText().toString());
    int num2 = Integer.parseInt(numero2.getText().toString());

    public void Calcular(View view){
        if(sumar.isChecked()){
            int sum = num1 + num2;
            resultado.setText(String.valueOf(sum));
        }
        else if(restar.isChecked()){
            int res = num1 - num2;
            resultado.setText(String.valueOf(res));

        }
        else if (sumar.isChecked() && restar.isChecked()){
            int sum = num1 + num2;
            int res = num1 - num2;
            resultado.setText(String.valueOf("La suma es: " + sum + "La resta es: " + res));
        }
    }
}